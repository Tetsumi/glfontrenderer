/*
 * Copyright 2014 Tetsumi <tetsumi@vmail.me>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tetsumi.glfontrenderer;


import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import java.awt.Font;

public class App 
{
    boolean openScreen () {
        try {
            Display.setDisplayMode(new DisplayMode(800, 600));
            Display.create();
        } catch (LWJGLException e) {
            return false;
        }
        return true;
    }
    
    void mainLoop () {
        Font font = new Font("Microsoft Sans Serif", Font.PLAIN, 16);
        FontRenderer fr = new FontRenderer(font);
        font = new Font("Courier", Font.PLAIN, 25);
        FontRenderer fr2 = new FontRenderer(font);
        
        GL11.glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
        
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0.0, 800, 0.0, 600, -1.0, 1.0);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);
        GL11.glLoadIdentity();
        GL11.glEnable(GL11.GL_TEXTURE_2D);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
        GL11.glEnable(GL11.GL_BLEND );
        
        while (!Display.isCloseRequested()) {
            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
            fr.render(50, 50, "ABCDEFGHIJKL");
            fr2.render(100, 100, "abcdefghijklmnopqrst");   
            fr.render(20, 400, "HELLO WORLD");   
            Display.update();
        }
        
        fr.dispose();
        fr2.dispose();
    }
    
    public static void main (String[] args)
    {
        System.out.println("glfontrenderer demo");
        App app = new App();
        if (app.openScreen())
            app.mainLoop();
        else
            System.exit(1);
    }
}
